function predict(X,y,num_labels,lambda)

    [all_theta] = oneVsAll(X, y, num_labels, lambda);%3x5
    fprintf('part 2.\n');
    fprintf('Program paused. Press enter to continue.\n');
    pred = predictOneVsAll(all_theta, X);
    fprintf('With lambda = %f.\n',lambda);
    fprintf('Training Set Accurary: %f\n', mean(double(pred == y)) * 100);
    fprintf('Program paused. Press enter to continue.\n');
    pause;
end