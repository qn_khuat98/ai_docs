%% Machine Learning Online Class - Exercise 3 | Part 1: One-vs-all

%  Instructions
%  ------------
%
%  This file contains code that helps you get started on the
%  linear exercise. You will need to complete the following functions
%  in this exericse:
%
%     lrCostFunction.m (logistic regression cost function)
%     oneVsAll.m
%     predictOneVsAll.m
%     predict.m
%
%  For this exercise, you will not need to change any code in this file,
%  or any other files other than those mentioned above.
%

%% Initialization
clear ; close all; clc

%% Setup the parameters you will use for this part of the exercise
num_labels = 3;           % 10 labels, from 1 to 10
                          % (note that we have mapped "0" to label 10)

%% =========== Part 1: Loading and Visualizing Data =============
%  We start the exercise by first loading and visualizing the dataset.
%  You will be working with a dataset that contains handwritten digits.
%

% Load Training Data
fprintf('Loading and Visualizing Data ...\n')
% 
% load('ex3data1.mat'); % training data stored in arrays X, y
% m = size(X, 1);

data=load('flowers.csv');
X=data(1:134,1:end-1);%134x4
y=data(1:134,end);%134x1
[m,n]=size(X);


Xtest=data(134:end,1:end-1);
ytest=data(134:end,end);

fprintf('Program paused. Press enter to continue.\n');
pause;

%% ============ Part 2a: Vectorize Logistic Regression ============
%  In this part of the exercise, you will reuse your logistic regression
%  code from the last exercise. You task here is to make sure that your
%  regularized logistic regression implementation is vectorized. After
%  that, you will implement one-vs-all classification for the handwritten
%  digit dataset.
%
 n
% Test case for lrCostFunction
fprintf('\nTesting lrCostFunction() with regularization');


theta = zeros(size(X,2)+1,1);


fprintf('Program paused. Press enter to continue.\n');
pause;
%% ============ Part 2b: One-vs-All Training ============
fprintf('\nTraining One-vs-All Logistic Regression...\n')
for i=0:0.1:1
    lambda = i;
    predict(X,y,num_labels,lambda);
    fprintf('\nTrain on test set\n')
    pause;
    predict(Xtest,ytest,num_labels,lambda);
    pause;
end


%% ================ Part 3: Predict for One-Vs-All ================

