%% Machine Learning Online Class - Exercise 1: Linear Regression

%  Instructions
%  ------------
%


%% Initialization
clear ; close all; clc;

%% ==================== Part 1: Load data ====================
% Load data codeex
fprintf('Loading data...\n');

data=load('flowers.csv');
x=data(1:134,1:end-1);%134x4
y=data(1:134,end);%134x1


xtest=data(135:end,1:end-1);%15x4
ytest=data(135:end,2);%15x1

% x=x_norm;
% type1 = find(y == 1); 
% type2 = find(y == 2);
% type3 = find(y == 3);
% 
% hold on;
% plot(x(type1,2), x(type1,4), 'k+','MarkerEdgeColor','r');
% plot(x(type2,2), x(type2,4), 'k+','MarkerEdgeColor','b');
% plot(x(type3,2), x(type3,4), 'k+','MarkerEdgeColor','y');
% hold off;


fprintf('Program paused. Press enter to continue.\n');
pause;


%% ======================= Part 2: feature scaling =======================
fprintf('Feature Scaling\n')

x_norm=x;
y_norm=y;
fprintf('Program paused. Press enter to continue.\n');
pause;

%% =================== Part 3: Train theta ===================


options = optimset('GradObj','on','Algorithm','trust-region','MaxIter',50);
lambda=0;
x_norm=[ones(size(y,1),1) x_norm];
theta = zeros(size(x_norm,2),1);

% for i =0:0.1:1
%     fprintf(['Lambda = %d.\n'],i);
%     lambda=i;
    [theta, cost] = fminunc(@(t)(lrCostFunction(t, x_norm, y_norm,lambda)),theta, options);
% end

fprintf('Program paused. Press enter to continue.\n');
pause;

%% ============= Part 4: Visualizing J(theta_0, theta_1) =============
% 
% pred = predictOneVsAll(theta, xcv_norm);
% 
% fprintf('\nTraining Set Accurary: %f\n', mean(double(pred == ycv_norm)) * 100);
